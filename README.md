Servidor Backend da Gestão de Atividades
---

## Tecnologias Usadas:

* JAVAX (Servlet)
* JUnit 4.12 (Testes Unitários)
* Hibernate 5.3.6
* JSON, GSON (Para Transporte de dados)
* Java Ws Rs (Rest API)
* Driver PostGreSql 9.1-901.jdbc4
* LOG4J (Para Gerenciamento de log's)
* Tomcat 9