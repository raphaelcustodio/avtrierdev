package service;

import java.util.List;

import Util.DateUtil;
import Util.ValueUtil;
import dao.AtividadeDao;
import domain.Atividade;
import exception.ValidationException;

public class AtividadeService {
	
	public static AtividadeService instance;
	
	public static final int QTLIMITECARACTERESFINALIZAATENDIMENTOURGENTE = 50;
	public static final int HOUR_LIMITE_INSERT_ATIVIDADE = 13;
	public static final int MINUTE_LIMITE_INSERT_ATIVIDADE = 00;
	
	public static AtividadeService getInstance() {
		if (ValueUtil.isEmpty(instance)) {
			instance = new AtividadeService();
		}
		return instance;
	}
	
	public Atividade findByPrimaryKey(Atividade atividadeFilter) throws ValidationException {
		if (ValueUtil.isEmpty(atividadeFilter) || ValueUtil.isEmpty(atividadeFilter.getCdAtividade())) throw new ValidationException("N�o foi recebido filtro para buscar pela chave prim�ria");
		return (Atividade) AtividadeDao.getInstance().findByPrimaryKey(atividadeFilter.getCdAtividade(), atividadeFilter);
	}

	public List<Atividade> findAllByExample(Atividade atividadeFilter) {
		return AtividadeDao.getInstance().findAllByExample(atividadeFilter);
	}

	public void insertAtividade(Atividade atividade) throws Exception {
		validateInsertAtividade(atividade);
		AtividadeDao.getInstance().insert(atividade);
	}
	
	public void updateAtividade(Atividade atividade) throws Exception {
		validateUpdateAtividade(atividade);
		AtividadeDao.getInstance().update(atividade);
	}

	public boolean removeAtividade(Atividade atividade) throws Exception {
		validateRemoverAtividade(atividade);
		atividade.setAtivo(!atividade.isAtivo());
		AtividadeDao.getInstance().update(atividade);
		return true;
	}
	
	public boolean finalizaAtividade(Atividade atividade) throws Exception {
		validateFinalizarAtividade(atividade);
		atividade.setFinalizada(!atividade.isFinalizada());
		AtividadeDao.getInstance().update(atividade);
		return true;
	}

	private void validateUpdateAtividade(Atividade atividade) throws ValidationException {
		if (ValueUtil.isEmpty(atividade)) throw new ValidationException("N�o foi recebido dados para realizar a edi��o da atividade");
		if (isManutencaoUrgenteAndIsValid(atividade)) throw new ValidationException("N�o foi possivel realizar a edi��o pois a data limite da manuten��o urgente n�o � v�lida, � poss�vel apenas cadastrar manuten��o urgente na sexta-feira at� as 13h!");
	}
	
	private void validateInsertAtividade(Atividade atividade) throws ValidationException {
		if (ValueUtil.isEmpty(atividade)) throw new ValidationException("N�o foi recebido dados para realizar o cadastro da atividade");
		if (isManutencaoUrgenteAndIsValid(atividade)) throw new ValidationException("N�o foi possivel realizar o cadastro pois a data limite da manuten��o urgente n�o � v�lida, � poss�vel apenas cadastrar manuten��o urgente na sexta-feira at� as 13h!");
	}

	private boolean isManutencaoUrgenteAndIsValid(Atividade atividade) {
		return atividade.isManutencaoUrgente() && isDateValidInsertManutencaoUrgente();
	}
	
	private boolean isDateValidInsertManutencaoUrgente() {
		 return DateUtil.isFriday() && !DateUtil.isTimeActualAfterEspecificTime(DateUtil.getCalendar(), HOUR_LIMITE_INSERT_ATIVIDADE, MINUTE_LIMITE_INSERT_ATIVIDADE);
	}


	private void validateRemoverAtividade(Atividade atividade) throws ValidationException {
		if (ValueUtil.isEmpty(atividade)) throw new ValidationException("N�o foi recebido dados para realizar a inativa��o da atividade!");
		if (atividade.isAtivo() && atividade.isManutencaoUrgente()) throw new ValidationException("N�o � poss�vel inativar uma manuten��o urgente!");
	}

	private void validateFinalizarAtividade(Atividade atividade) throws ValidationException {
		if (ValueUtil.isEmpty(atividade)) throw new ValidationException("N�o foi recebido dados para finalizar a atividade!");
		if (!atividade.isFinalizada() && isAtendimentoOuManutencaoUrgente(atividade) && atividade.getDsAtividade().length() < QTLIMITECARACTERESFINALIZAATENDIMENTOURGENTE) {
			throw new ValidationException("N�o foi poss�vel finalizar a atividade, pois atividades de atendimento ou manuten��o urgente devem ter uma descri��o de pelo menos 50 caracteres!");
		}
	}
	
	public boolean isAtendimentoOuManutencaoUrgente(Atividade atividade) {
		return atividade.isManutencaoUrgente() || atividade.isAtendimento();
	}

}
