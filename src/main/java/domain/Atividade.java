package domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Type;

import enums.TipoAtividadeEnum;

@Entity
@Table(name="Atividade")
public class Atividade extends BaseDomain {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column
	private Integer cdAtividade;
	
	@Type(type="string")
	@Column
	private String nmTitulo;
	
	@Type(type="string")
	@Column
	private String dsAtividade;
	
	@Type(type="integer")
	@Column
	private int cdTipoAtividade;
	
	@Type(type="yes_no")
	@Column(name="FLFINALIZADA")
	private boolean isFinalizada;
	
	@Type(type="yes_no")
	@Column(name="FLATIVO")
	private boolean isAtivo;
	
	@Transient
	private boolean isIgnoreFlAtivoFilter;
	@Transient
	private boolean isIgnoreFlFinalizadaFilter;
	
	public Integer getCdAtividade() {
		return cdAtividade;
	}
	
	public void setCdAtividade(final Integer cdAtividade) {
		this.cdAtividade = cdAtividade;
	}
	
	public String getNmTitulo() {
		return nmTitulo;
	}
	
	public void setNmTitulo(final String nmTitulo) {
		this.nmTitulo = nmTitulo;
	}
	
	public String getDsAtividade() {
		return dsAtividade;
	}
	
	public void setDsAtividade(final String dsAtividade) {
		this.dsAtividade = dsAtividade;
	}
	
	public int getCdTipoAtividade() {
		return cdTipoAtividade;
	}
	
	public void setCdTipoAtividade(final int tpAtividade) {
		this.cdTipoAtividade = tpAtividade;
	}
	
	public boolean isFinalizada() {
		return isFinalizada;
	}
	
	public void setFinalizada(final boolean isFinalizada) {
		this.isFinalizada = isFinalizada;
	}

	public boolean isAtivo() {
		return isAtivo;
	}

	public void setAtivo(final boolean isAtivo) {
		this.isAtivo = isAtivo;
	}
	
	public boolean isManutencaoUrgente() {
		return TipoAtividadeEnum.MANUTENCAO_URGENTE.valor == this.cdTipoAtividade;
	}
	
	public boolean isAtendimento() {
		return TipoAtividadeEnum.ATENDIMENTO.valor == this.cdTipoAtividade;
	}

	public boolean isIgnoreFlAtivoFilter() {
		return isIgnoreFlAtivoFilter;
	}

	public void setIgnoreFlAtivoFilter(boolean isIgnoreFlAtivoFilter) {
		this.isIgnoreFlAtivoFilter = isIgnoreFlAtivoFilter;
	}

	public boolean isIgnoreFlFinalizadaFilter() {
		return isIgnoreFlFinalizadaFilter;
	}

	public void setIgnoreFlFinalizadaFilter(boolean isIgnoreFlFinalizadaFilter) {
		this.isIgnoreFlFinalizadaFilter = isIgnoreFlFinalizadaFilter;
	}

}
