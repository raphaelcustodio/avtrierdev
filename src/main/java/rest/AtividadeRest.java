package rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.glassfish.jersey.internal.util.Base64;

import com.google.gson.Gson;

import domain.Atividade;
import service.AtividadeService;

@Path("AtividadeRest")
public class AtividadeRest extends AbstractRest {
	
	protected static final Logger parentLogger = LogManager.getLogger();
	private Logger logger = parentLogger;
	
	@GET
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	@Path("findByPrimaryKey/{cdAtividade}")
	public Response findByPrimaryKey(@PathParam("cdAtividade") String id) {
		try {
			Atividade atividadeFilter = new Atividade();
			atividadeFilter.setCdAtividade(Integer.parseInt(Base64.decodeAsString(id)));
			return super.getResponseOk(new Gson().toJson(AtividadeService.getInstance().findByPrimaryKey(atividadeFilter)).toString());
		} catch (Exception e) {
			String error = super.handleException("findByPrimaryKey", this.getClass().getSimpleName(), e);
			logger.error(error);
			return super.getResponseError(error);
		}
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	@Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
	@Path("findAllByExample/{jsonFilter}")
	public Response findAllByExample(@PathParam("jsonFilter") String jsonFilter) {
		try {
			return super.getResponseOk(new Gson().toJson(AtividadeService.getInstance().findAllByExample(new Gson().fromJson(Base64.decodeAsString(jsonFilter), Atividade.class))).toString());
		} catch (Exception e) {
			String error = super.handleException("findAllByExample", this.getClass().getSimpleName(), e);
			logger.error(error);
			return super.getResponseError(error);
		}
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED + ";charset=utf-8")
	@Path("/insert")
	public Response insertAtividade(String data) {
		try {
			AtividadeService.getInstance().insertAtividade(new Gson().fromJson(data, Atividade.class));
			return super.getResponseOk(super.prepareRetorno("OK"));
		} catch (Exception e) {
			String error = super.handleException("insertAtividade", this.getClass().getSimpleName(), e);
			logger.error(error);
			return super.getResponseError(error);
		}
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED + ";charset=utf-8")
	@Path("/update")
	public Response updateAtividade(String data) {
		try {
			AtividadeService.getInstance().updateAtividade(new Gson().fromJson(data, Atividade.class));
			return super.getResponseOk(super.prepareRetorno("OK"));
		} catch (Exception e) {
			String error = super.handleException("updateAtividade", this.getClass().getSimpleName(), e);
			logger.error(error);
			return super.getResponseError(error);
		}
	}
	
	
	@POST
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED + ";charset=utf-8")
	@Path("/finalizar")
	public Response finalizarAtividade(String data) {
		try {
			AtividadeService.getInstance().finalizaAtividade(new Gson().fromJson(data, Atividade.class));
			return super.getResponseOk(super.prepareRetorno("OK"));
		} catch (Exception e) {
			String error = super.handleException("updateAtividade", this.getClass().getSimpleName(), e);
			logger.error(error);
			return super.getResponseError(error);
		}
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED + ";charset=utf-8")
	@Path("/remove")
	public Response removeAtividade(String data) {
		return processActivationAtividade(data);
	}

	private Response processActivationAtividade(String data) {
		try {
			Atividade atividade = new Gson().fromJson(data, Atividade.class);
			AtividadeService.getInstance().removeAtividade(atividade);
			return super.getResponseOk(super.prepareRetorno("OK"));
		} catch (Exception e) {
			String error = super.handleException("updateAtividade", this.getClass().getSimpleName(), e);
			logger.error(error);
			return super.getResponseError(error);
		}
	}

}
