package rest;

import java.util.Date;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import org.json.JSONObject;

import exception.ValidationException;

@Path("AbstractRest")
public class AbstractRest {
	
	@GET
	@Path("/echo")
	@Produces("application/json")
	public Response echo() {
		try {
			JSONObject jsonEcho = new JSONObject();
			jsonEcho.put("serverStatus", "online");
			jsonEcho.put("serverDate", new Date().toString());
			return getResponseOk(jsonEcho.toString());
		} catch (Exception e) {
			return getResponseError(prepareRetorno(handleException("findAll", this.getClass().getSimpleName(), e)));
		}
	}
	
	protected String handleException(String methodName, String className, Exception e) {
		if (e instanceof ValidationException) {
			return e.getMessage();
		}
		return "Erro ao realizar o "+methodName+" da classe: "+className+", Detalhes: " + e.getMessage();
	}
	
	protected String prepareRetorno(String message) {
		JSONObject jsonRetorno = new JSONObject();
		jsonRetorno.put("retorno", message);
		return jsonRetorno.toString();
	}
	
	protected Response getResponseOk(String jsonString) {
		return Response.ok()
		.entity(jsonString)
		.header("Access-Control-Allow-Origin", "*")
		.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT")
		.allow("OPTIONS").build();
	}
	
	protected Response getResponseError(String jsonError) {
		return Response.serverError()
				.entity(prepareRetorno(jsonError))
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT")
				.allow("OPTIONS").build();
	}

}
