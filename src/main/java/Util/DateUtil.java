package Util;

import java.util.Calendar;
import java.util.Date;

public class DateUtil {
	
	public static boolean isFriday() {
		Calendar c = getCalendar();
		c.setTime(new Date());
		return c.get(Calendar.DAY_OF_WEEK) == Calendar.FRIDAY;
	}
	
	public static Calendar getCalendar() {
		return Calendar.getInstance();
	}
	
	public static boolean isTimeActualAfterEspecificTime(Calendar calendarActual, int hour, int minute) {
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.HOUR_OF_DAY, hour);
		calendar.set(Calendar.MINUTE, minute);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		return calendarActual.compareTo(calendar) >= 0;
	}

	
	
}
