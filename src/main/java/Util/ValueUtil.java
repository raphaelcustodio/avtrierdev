package Util;

public class ValueUtil {
	
	public static boolean isEmpty(Object obj) {
		return obj == null;
	}
	
	public static boolean isNotEmpty(Object obj) {
		return !isEmpty(obj);
	}
	
	public static boolean isEmpty(String value) {
		return value == null || value.isEmpty();
	}
	
	public static boolean isNotEmpty(String value) {
		return !isEmpty(value);
	}

}
