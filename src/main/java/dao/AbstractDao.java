package dao;

import java.util.List;

import javax.persistence.EntityManager;

import Util.ValueUtil;
import domain.BaseDomain;
import listeners.EntityManagerInstance;

public abstract class AbstractDao {
	
	protected EntityManager entityManager;
	
	protected EntityManager getEntityManagerInstance(BaseDomain baseClass) {
		return EntityManagerInstance.entityManager;
	}
	
	public BaseDomain findByPrimaryKey(final int id, final BaseDomain domainFilter) {
		if (ValueUtil.isEmpty(domainFilter)) return null;
        return getEntityManagerInstance(domainFilter).find(domainFilter.getClass(), id);
	}
	
	public List<?> findAll(final BaseDomain domainFilter) {
		if (ValueUtil.isEmpty(domainFilter)) return null;
        return getEntityManagerInstance(domainFilter).createQuery("FROM " + domainFilter.getClass().getSimpleName()).getResultList();
	}
	
	public void insert(final BaseDomain domain) throws Exception {
		if (ValueUtil.isEmpty(domain)) return;
		try {
			getEntityManagerInstance(domain).getTransaction().begin();
			getEntityManagerInstance(domain).persist(domain);
			getEntityManagerInstance(domain).getTransaction().commit();
		} catch (Exception ex) {
			getEntityManagerInstance(domain).getTransaction().rollback();
			throw ex;
		}
		
	}
	
	public void update(final BaseDomain domain) throws Exception {
		if (ValueUtil.isEmpty(domain)) return;
		try {
			getEntityManagerInstance(domain).getTransaction().begin();
			getEntityManagerInstance(domain).merge(domain);
			getEntityManagerInstance(domain).getTransaction().commit();
		} catch (Exception ex) {
			getEntityManagerInstance(domain).getTransaction().rollback();
			throw ex;
		}
		
	}
	
	public void remove(final int id, BaseDomain domain) throws Exception {
		if (ValueUtil.isEmpty(domain)) return;
		try {
			getEntityManagerInstance(domain).getTransaction().begin();
			domain = getEntityManagerInstance(domain).find(domain.getClass(), id);
			getEntityManagerInstance(domain).remove(domain);
			getEntityManagerInstance(domain).getTransaction().commit();
		} catch (Exception ex) {
			getEntityManagerInstance(domain).getTransaction().rollback();
			throw ex;
		}
		
	}

	
}
