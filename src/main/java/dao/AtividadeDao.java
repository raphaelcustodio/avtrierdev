package dao;

import java.util.LinkedList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import Util.ValueUtil;
import domain.Atividade;

public class AtividadeDao extends AbstractDao {
	
	public static AtividadeDao instance;
	
	public static AtividadeDao getInstance() {
		if (ValueUtil.isEmpty(instance)) {
			instance = new AtividadeDao();
		}
		return instance;
	}

	public List<Atividade> findAllByExample(Atividade atividadeFilter) {
		CriteriaBuilder builder = getEntityManagerInstance(atividadeFilter).getCriteriaBuilder();
		CriteriaQuery<Atividade> atividadeCriteria = builder.createQuery(Atividade.class);
		Root<Atividade> atividadeRoot = atividadeCriteria.from(Atividade.class);
		atividadeCriteria.select(atividadeRoot);
		List<Predicate> predList = new LinkedList<Predicate>();
		if (ValueUtil.isNotEmpty(atividadeFilter.getCdAtividade())) {
			predList.add(builder.equal(atividadeRoot.get("cdAtividade"), atividadeFilter.getCdAtividade()));
		}
		if (ValueUtil.isNotEmpty(atividadeFilter.getNmTitulo())) {
			predList.add(builder.like(atividadeRoot.get("nmTitulo"), "%"+atividadeFilter.getNmTitulo()+"%"));
		}
		if (ValueUtil.isNotEmpty(atividadeFilter.getDsAtividade())) {
			predList.add(builder.like(atividadeRoot.get("dsAtividade"), "%"+atividadeFilter.getDsAtividade()+"%"));
		}
		if (atividadeFilter.getCdTipoAtividade() > 0) {
			predList.add(builder.equal(atividadeRoot.get("cdTipoAtividade"), atividadeFilter.getCdTipoAtividade()));
		}
		if (!atividadeFilter.isIgnoreFlFinalizadaFilter()) {
			predList.add(builder.equal(atividadeRoot.get("isFinalizada"), atividadeFilter.isFinalizada()));
		}
		if (!atividadeFilter.isIgnoreFlAtivoFilter()) {
			predList.add(builder.equal(atividadeRoot.get("isAtivo"), atividadeFilter.isAtivo()));
		}
		Predicate[] predArray = new Predicate[predList.size()];
		predList.toArray(predArray);
		atividadeCriteria.where(predArray);
		atividadeCriteria.orderBy(builder.asc(atividadeRoot.get("cdAtividade")));
		return getEntityManagerInstance(atividadeFilter).createQuery(atividadeCriteria).getResultList();
	}
	
}
