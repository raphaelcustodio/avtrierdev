package listeners;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import Util.ValueUtil;

public class EntityManagerInstance implements ServletContextListener {
	
	public static EntityManager entityManager;

	@Override
	public void contextInitialized(ServletContextEvent sce) {
		if (ValueUtil.isEmpty(entityManager)) {
			EntityManagerFactory factory = Persistence.createEntityManagerFactory("AvDevTrierPU");
            entityManager = factory.createEntityManager();
        }
	}

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		
	}

}
