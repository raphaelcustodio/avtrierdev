package enums;

public enum TipoAtividadeEnum {
	
	DESENVOLVIMENTO(1),
	ATENDIMENTO(2),
	MANUTENCAO(3),
	MANUTENCAO_URGENTE(4);
	
	public int valor;
	private TipoAtividadeEnum(int valor) {
		this.valor = valor;
	}

}
