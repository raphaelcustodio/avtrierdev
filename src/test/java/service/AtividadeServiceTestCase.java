package service;

import domain.Atividade;
import enums.TipoAtividadeEnum;
import junit.framework.TestCase;

public class AtividadeServiceTestCase extends TestCase {

	public void testIsManutencaoUrgenteAndIsValid() {
		Atividade atividade = new Atividade();
		assertFalse(AtividadeService.getInstance().isAtendimentoOuManutencaoUrgente(atividade));
		atividade.setCdTipoAtividade(TipoAtividadeEnum.DESENVOLVIMENTO.valor);
		assertFalse(AtividadeService.getInstance().isAtendimentoOuManutencaoUrgente(atividade));
		atividade.setCdTipoAtividade(TipoAtividadeEnum.MANUTENCAO.valor);
		assertFalse(AtividadeService.getInstance().isAtendimentoOuManutencaoUrgente(atividade));
		atividade.setCdTipoAtividade(TipoAtividadeEnum.ATENDIMENTO.valor);
		assertTrue(AtividadeService.getInstance().isAtendimentoOuManutencaoUrgente(atividade));
		atividade.setCdTipoAtividade(TipoAtividadeEnum.MANUTENCAO_URGENTE.valor);
		assertTrue(AtividadeService.getInstance().isAtendimentoOuManutencaoUrgente(atividade));
	}
	
}
