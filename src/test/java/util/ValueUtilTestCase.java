package util;

import org.junit.Test;

import Util.ValueUtil;
import junit.framework.TestCase;

public class ValueUtilTestCase extends TestCase{
	
	@Test
	public void testIsEmpty() {
		assertTrue(ValueUtil.isEmpty(null));
		assertFalse(ValueUtil.isEmpty(new Object()));
		
		String test = null;
		assertTrue(ValueUtil.isEmpty(test));
		test = "";
		assertTrue(ValueUtil.isEmpty(test));
		test = "Test String";
		assertFalse(ValueUtil.isEmpty(test));
		test = "1";
		assertFalse(ValueUtil.isEmpty(test));
		test = " ";
		assertFalse(ValueUtil.isEmpty(test));
	}
	
	@Test
	public void testIsNotEmpty() {
		assertFalse(ValueUtil.isNotEmpty(null));
		assertTrue(ValueUtil.isNotEmpty(new Object()));
		
		String test = null;
		assertFalse(ValueUtil.isNotEmpty(test));
		test = "";
		assertFalse(ValueUtil.isNotEmpty(test));
		test = "Test String";
		assertTrue(ValueUtil.isNotEmpty(test));
		test = "1";
		assertTrue(ValueUtil.isNotEmpty(test));
		test = " ";
		assertTrue(ValueUtil.isNotEmpty(test));
	}

}
