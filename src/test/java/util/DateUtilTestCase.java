package util;

import java.util.Calendar;

import org.junit.Test;

import Util.DateUtil;
import junit.framework.TestCase;

public class DateUtilTestCase extends TestCase {
	
	@Test
	public void testIsTimeActualAfterEspecificTime() {
		assertTrue(DateUtil.isTimeActualAfterEspecificTime(getCalendarTest(22, 46), 22, 46));
		assertTrue(DateUtil.isTimeActualAfterEspecificTime(getCalendarTest(22, 46), 22, 45));
		assertFalse(DateUtil.isTimeActualAfterEspecificTime(getCalendarTest(22, 46), 22, 47));
	}

	private Calendar getCalendarTest(int hour, int minute) {
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.HOUR_OF_DAY, hour);
		calendar.set(Calendar.MINUTE, minute);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		return calendar;
	}

}
